FROM alpine:3.14
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} asyncreportprocessorcloud-1.1.0.jar
#COPY logback-spring.xml logback-spring.xml

#ENTRYPOINT ["java","-Dspring.profiles.active=cloud","-jar","/asyncreportprocessorcloud-1.1.0.jar"]
RUN apk update && apk add bash curl --no-cache && rm -rf /var/cache/apk/*

EXPOSE 17000

ARG APP_NAME
ARG APP_VERSION

ENV APP_HOME /opt/SP/apps/${APP_NAME}

RUN mkdir -p $APP_HOME

WORKDIR $APP_HOME

# Create a  user
ENV USER_NAME 1001
RUN adduser -S ${USER_NAME} --uid ${USER_NAME} && chown -R ${USER_NAME}:${USER_NAME} ${APP_HOME}
USER ${USER_NAME}

RUN echo `pwd`
RUN echo `ls -la`

COPY ./target/${APP_NAME}-${APP_VERSION}.jar app.jar

ENV JAVA_OPTS=""

ENTRYPOINT ["sh", "-c"]
CMD ["exec java $JAVA_OPTS -jar app.jar"]
